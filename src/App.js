import './App.css';
import {Popover} from "./popover";

function App() {
  const content = (
    <div className='parent-content'>
      <header>Hello header</header>
      <main>Hello main Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab accusantium ad alias animi
        blanditiis doloremque exercitationem labore, maiores molestias, necessitatibus nemo odio perferendis praesentium
        quidem, quod quos rem tenetur.
      </main>
      <footer>Hello footer</footer>
    </div>
  );


  return (
    <div className="App">
      <Popover content={content} defaultPosition='bottom' styles={{
        width: '500px'
      }}>
        <button className='btn'>Call popover!</button>
      </Popover>

    </div>

  );
}

export default App;
