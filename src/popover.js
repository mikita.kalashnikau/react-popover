import './Popover.css';
import React, {useState, useRef, useEffect} from "react";

export function Popover({content, children, defaultPosition = 'bottom', styles}) {
  const classes = {
    bottom: 'content content-bottom',
    top: 'content content-top',
    left: 'content content-left',
    right: 'content content-right'
  };
  const [isShowContent, setIsShowContent] = useState(false);
  const [position, setPosition] = useState(defaultPosition);
  const contentRef = useRef(null);
  const childrenRef = useRef(null);

  const isInViewport = (element) => {
    const rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  };

  const documentScrollHandler = (event) => {
    if (contentRef.current && !isInViewport(contentRef.current)) {
      changePosition();
    }
  };

  const clickOutside = (event) => {
    if (!contentRef.current?.contains(event.target) && !childrenRef.current.contains(event.target)) {
      document.removeEventListener('mousedown', clickOutside);
      setIsShowContent(false);
      setPosition(defaultPosition);
    }
  };

  const openPopoverHandler = () => {
    if (!isShowContent) {
      setIsShowContent(true);
      document.addEventListener('mousedown', clickOutside);
      document.addEventListener('scroll', documentScrollHandler);
    } else {
      setIsShowContent(false);
      setPosition(defaultPosition);
    }
  };

  const changePosition = () => {
    const rect = contentRef.current.getBoundingClientRect();

    if (rect.left <= 0) {
      setPosition('right')
    }

    if (rect.right >= (window.innerWidth || document.documentElement.clientWidth)) {
      setPosition('left')
    }

    if (rect.top <= 0) {
      setPosition('bottom')
    }

    if (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight)) {
      setPosition('top')
    }
  };

  useEffect(() => {
    if (contentRef.current && !isInViewport(contentRef.current)) {
      changePosition();
    }
  }, [contentRef.current, isShowContent]);

  return (
    <div className='container'>
      <div className="children" ref={childrenRef} onClick={openPopoverHandler}>
        {children}
      </div>
      {
        isShowContent ?
          <div className={classes[position]} ref={contentRef} style={styles}>
            {content}
          </div>
          : ''
      }
    </div>
  )
}
